#! /usr/bin/env node

/**
* A Rock Paper Scissor game demo
*
* NOTE: To update the game to make it 4 players, include
* a new player in 'choices' object and update init function
* to include the player and scenarios
*
* @author Prasanna K Yalala
*/

"use strict";

// imports
var inquirer = require("inquirer");
var rps = require("./lib/rps-core");
var rpsUI = require("./lib/rps-ui");
var Console = rpsUI.Console;
var deal = rpsUI.deal;
var confirmReplay = {
	type: "confirm",
	name: "repeatPlay",
	message: "Would you like to play again?",
	default: false
};

// populate data
rps.init();

// start enquire
var start = function(){
	inquirer.prompt(deal, function(response){
		var selection = response.selection;
		var resultObject = rps.throw(selection);
		var opponentSelection = resultObject.opponent,
		    result = resultObject.result;
		Console.customMessage("Computer picked", opponentSelection);
		Console.result(result);
		
		inquirer.prompt(confirmReplay, function(response){
			if(response.repeatPlay === true){
				start();
			}else{
				process.exit(0);
			}
		});
	});
}

start();
