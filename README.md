# README #

A simple Rock Paper Scissors game I've re purposed from an older JavaScript code which I had lying around. This now uses Node.JS to run, so make sure that you have it installed!

### What is this repository for? ###

* A simple Rock Paper Scissor game
* v0.0.1

### How do I get set up? ###

* Summary of set up

Install Node.JS as per [official instructions](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager) if you don't have it already.

* clone the repo in to a directory (let's say gitrepos/rps) and install the dependencies just by typing 
```
#!node

npm install -g
```

That should install everything (if you are running a linux machine, you may have to use sudo)

 * Just begin playing by typing 
```
#!commandline

rps
```


### Questions/Comments? ###

* contact me via bitbucket portal