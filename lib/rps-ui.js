/**
* A Rock Paper Scissor game demo
*
* NOTE: To update the game to make it 4 players, include
* a new player in 'choices' object and update init function
* to include the player and scenarios
*
* @author Prasanna K Yalala
*/

"use strict";

var Chalk = require("chalk");

// Game choices
var choices = [{
		name: "Rock",
		checked: false
	},{
		name: "Paper",
		checked: false
	},{
		name: "Scissors",
		checked: false
}];

// inquirer interactive Object 
module.exports.deal = {
	type: "list",
	name: "selection",
	message: "Pick your option",
	choices: choices
};

module.exports.Console = {
	customMessage : function(msg, item){
		console.log(Chalk.green("* ")+Chalk.white.bold(msg) +": "+ Chalk.cyan(item));
	},
	result: function(outcome){
		switch(outcome){
			case "Win":
				console.log(Chalk.green.bold("Congrats, You've Won"));
				break;
			case "Lost":
				console.log(Chalk.red.bold("Bad Luck, You've Lost"));
				break;
			case "Draw": 
				console.log(Chalk.yellow.bold("It's a Draw"));
				break;
			default: 
				break;
		}
	}
};

