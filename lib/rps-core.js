
/**
* A Rock Paper Scissor game demo core
*
* NOTE: To update the game to make it 4 players, update
* init function to include the player and scenarios
*
* @author Prasanna K Yalala
*/

'use strict';

/**
* Holds an object of items and their associated enemies
* 
* @property beatenBy
* @type {Object}
* @default {}
*/
var beatenBy = {};

/**
* Holds a list of items
* 
* @property items
* @type {Array}
* @default []
*/
var items = new Array();

/**
* returns a string with its first character capitalized
*
* @method capitalize
* @return {String} Returns a string with its first letter capitalized
*/
String.prototype.capitalize = function(){
    return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); });
};

/**
* sets default values to properties
*
* @method init
*/
module.exports.init = function(){
	items[1] = "rock",
	items[2] = "paper",
	items[3] = "scissors",
	beatenBy = { "rock" : "paper", "paper" : "scissors", "scissors" : "rock"};
};

/**
* returns a random number between 1 and the length of items
*
* @method randomizeSelection
* @return {Integer} Returns a number
*/
var randomizeSelection = function() {
	var min = 1, max = items.length-1;
	return Math.floor(Math.random() * max) + min;
};

/**
* contains core logic for the game
*
* @method throw
* @param {String} thrownHand an Item picked by the user
* @return {Object} result Returns result 
*/
module.exports.throw = function(thrownHand){
	var selection = thrownHand.toLowerCase();
	var compSelectionIndex = randomizeSelection();
	var compSelection = items[compSelectionIndex];
	var userEnemy = beatenBy[selection],
	    result = {};
	// determine AI's enemy
	for(var x in beatenBy){
		if(x === selection){
			var compEnemy = beatenBy[x];
		}
	}
	// if Item selected by user is similar to Item selected by AI
	if (selection === compSelection) {
		result = {
			"selection": selection,
			"opponent": compSelection.capitalize(),
			"result": "Draw"
		};
	// if User's enemy is similar to Item selected by AI
	} else if (userEnemy === compSelection) {
		result = {
			"selection": selection,
			"opponent": compSelection.capitalize(),
			"result": "Lost"
		};
	} else { 
		for(var y in beatenBy){
			if(beatenBy[y] === selection){
				var userSuccess = y;
			}
		}
		// if User's Item is an enemy of an Item selected by AI
		if(selection === compEnemy || userSuccess === compSelection){
			result = {
				"selection": selection,
				"opponent": compSelection.capitalize(),
				"result": "Win"
			};
		}
	}
	return result;
};
